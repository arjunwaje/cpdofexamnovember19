package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class AboutCPDOFTest {

    @Test
    public void testDesc(){
        String str = new AboutCPDOF().desc();
        Assert.assertTrue(str.contains("CP-DOF certification program"));
    }
}
