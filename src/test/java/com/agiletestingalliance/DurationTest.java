package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class DurationTest {

    @Test
    public void testDur(){
        String str = new Duration().fnDuration();
        Assert.assertTrue(str.contains("CP-DOF is designed"));
    }
}
