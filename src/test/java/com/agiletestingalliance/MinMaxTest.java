package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testReturnMax01(){
        int i = new MinMax().returnMax(2,3);
        Assert.assertEquals("Max1", 3, i);
    }

    @Test
    public void testReturnMax02(){
        int i = new MinMax().returnMax(3,2);
        Assert.assertEquals("Max2", 3, i);
    }
}
