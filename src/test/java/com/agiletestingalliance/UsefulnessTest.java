package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class UsefulnessTest {

    @Test
    public void testDesc(){
        String str = new Usefulness().desc();
        Assert.assertTrue(str.contains("DevOps is about transformation"));
    }
}
